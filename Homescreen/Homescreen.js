import React, {Component} from 'react';
import {Button, View, Text} from 'react-native';
import {StackNavigator } from 'react-navigation';
import Page2 from '../Page2/Page2';
//import {Post} from './Post/Post';

class Homescreen extends Component {
  
  static navigationOptions = ({ navigation, navigationOptions }) => {
    //const { params } = navigation.state;
    return{
      headerRight: (
        <Button title="Post" color="#fff" onPress= {()=> {navigation.navigate('Page2')}} />
 
      ),
    };
  }

  render () {
    return (
      <View style={{padding: 50}}>
        <Text>
          Home screen!!!!  
        </Text>
      </View>
    );
  }
}
const Stack=StackNavigator(
  {
    Home:{
      screen:Homescreen,
    },
    // Page2: {
    //   screen: Page2,
    // }
  },

  {
    initialRouteName: 'Home',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }

);
export default Stack;



